# ASSIGNMENT: Robust Back-end Web Application

## Overview
This project focuses on building a robust back-end for a web application that handles data storage, retrieval, and authorization. The application is developed using the Flask framework with Python, and utilizes SQLite for database management. The core functionalities include creating, reading, updating, and deleting (CRUD) operations for website pages, user authentication and authorization, data validation, error handling, and security measures 

## Table of Contents
1. [Installation](#installation)
2. [Database Schema](#database-schema)
3. [API Endpoints](#api-endpoints)
4. [Data Validation](#data-validation)
5. [Error Handling](#error-handling)
6. [Security Measures](#security-measures)
7. [Documentation](#documentation)


## Installation
- Ensure you have Python 3.x installed.
- Install the required packages: `pip install -r requirements.txt`
- Set up the database: `flask db init`, `flask db migrate`, `flask db upgrade`
- Run the application: `python run.py`

## Database Schema
The database schema consists of two main models - `Page` and `User`. The `Page` model stores data related to website pages including title, description, metadata, status, and more. The `User` model handles user data for authentication purposes.

## API Endpoints
The application provides a set of RESTful API endpoints for managing website pages and user authentication:

### Page Endpoints:
- `POST /pages`: Create a new page with the provided data.
- `GET /pages`: Retrieve a list of all pages, with options for pagination and sorting.
- `GET /pages/<int:id>`: Retrieve the details of a specific page by its ID.
- `PUT /pages/<int:id>`: Update the details of a specific page by its ID with the provided data.
- `DELETE /pages/<int:id>`: Delete a specific page by its ID.
- `PUT /pages/<int:id>/activate`: Activate a specific page by its ID, changing its status to active.
- `PUT /pages/<int:id>/deactivate`: Deactivate a specific page by its ID, changing its status to inactive.

### User Endpoints:
- `POST /register`: Register a new user with the provided username, email, and password.
- `POST /login`: Authenticate a user and return a JWT token for authorized requests.

Each endpoint may have different authentication and authorization requirements, as well as data validation schemas to ensure the integrity and security of the data.

## Data Validation
Data validation is carried out at the server side using Marshmallow schemas to ensure data integrity and security.

## Error Handling
Custom error handlers provide meaningful error responses to the front-end, aiding in debugging and ensuring a robust user experience.

## Security Measures
Security measures include protections against CSRF (Cross-Site Request Forgery) and XSS (Cross-Site Scripting) attacks using the Flask-Talisman and Flask-SeaSurf libraries.

## Documentation

### Swagger Documentation:
The API documentation is available through Swagger, which provides an interactive interface for exploring the API, understanding the request/response models, and even testing the endpoints directly from the documentation.

- Access the Swagger documentation at: `/docs`

### Authorization:
Authorization is handled using JWT (JSON Web Tokens) through the Flask-JWT-Extended library. Upon successful login, a JWT token is provided which should be included in the Authorization header for requests to protected endpoints.

- To authorize, use the `POST /login` endpoint with your credentials to obtain a JWT token.
- Include the token in the Authorization header for subsequent requests to protected endpoints, formatted as `Bearer <token>`.

The Swagger documentation provides an "Authorize" button at the top right, where you can enter your JWT token to authorize the Swagger UI to make requests on your behalf.

In the Swagger documentation, protected endpoints are indicated, and you can use the "Try it out" feature to send requests with the Authorization header once you have entered your token.

