from app.extensions import ma
from app.models.page import Page
from marshmallow import fields, validate, Schema, ValidationError, validates
from app.schemas.custom_fields import DateTimeField
from datetime import datetime


class PageSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Page
        load_instance = True
    title = fields.Str(required=True, validate=validate.Length(max=300))
    meta_description = fields.Str(validate=validate.Length(max=200))
    meta_title = fields.Str(validate=validate.Length(max=200))
    meta_tag = fields.Str(validate=validate.Length(max=500))
    name = fields.Str(validate=validate.Length(max=100))
    description = fields.Str(validate=validate.Length(max=500))
    status = fields.Bool()
    namespaces = fields.List(fields.Str(), validate=validate.Length(
        min=1, max=100))
    body = fields.Str()
    namespaces = fields.List(fields.String(), required=False)
    create_date = DateTimeField(required=False)
    modified_date = DateTimeField(required=False)
    published_date = DateTimeField(required=False)

    @validates('modified_date')
    def validate_published_date(self, value):
        if value and value < datetime.utcnow():
            raise ValidationError('Published date cannot be in the past.')

    @validates('title')
    def validate_unique_title(self, value):
        if Page.query.filter_by(title=value).first():
            raise ValidationError('Title must be unique.')

    @validates('name')
    def validate_unique_name(self, value):
        if Page.query.filter_by(name=value).first():
            raise ValidationError('Name must be unique.')

    @validates('namespaces')
    def validate_namespaces(self, value):
        if not value:
            return
        if len(value) > 50:
            raise ValidationError(
                'Namespaces list can have a maximum of 50 items.')


page_schema = PageSchema()
pages_schema = PageSchema(many=True)
