from app.extensions import ma
from app.models.user import User
from marshmallow import fields


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User

    username = fields.String(unique=True)
    password = fields.String(load_only=True)
    email = fields.Email(required=True, unique=True)
