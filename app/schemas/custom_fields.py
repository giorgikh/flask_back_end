from marshmallow import fields, ValidationError
from dateutil.parser import parse


class DateTimeField(fields.Field):
    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return parse(value)
        except ValueError:
            raise ValidationError('Not a valid datetime.')
