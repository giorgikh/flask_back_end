from app.extensions import db
from datetime import datetime


class Page(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(300), nullable=False)
    meta_description = db.Column(db.String(200))
    meta_title = db.Column(db.String(200))
    meta_tag = db.Column(db.String(500))
    name = db.Column(db.String(100))
    description = db.Column(db.String(500))
    status = db.Column(db.Boolean, default=True)
    namespaces = db.Column(db.PickleType)
    create_date = db.Column(db.DateTime, default=datetime.utcnow)
    modified_date = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    published_date = db.Column(db.DateTime)
    body = db.Column(db.Text)
