from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_talisman import Talisman
from flasgger import Swagger
from app.config import Config
from app.models import page, user
from app.extensions import jwt
from app.routes import user_routes, page_routes, error_handlers
from app.extensions import db, ma

app = Flask(__name__)
app.config.from_object(Config)


def create_app(config_class=Config):
    jwt.init_app(app)
    db.init_app(app)
    ma.init_app(app)
    talisman = Talisman(app, content_security_policy=None)
    swagger = Swagger(app, template={
        "swagger": "2.0",
        "info": {
            "version": "1.0.0",
            "title": "Your API Title",
            "description": "For all endpoints requiring authentication, use the format **'Bearer YOUR_TOKEN_HERE'** in the Authorization header."
        },
        "securityDefinitions": {
            "Bearer": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header"
            }
        },
        "security": [
            {
                "Bearer": []
            }
        ]
    }, config={
        "headers": [],
        "specs": [{
            "endpoint": 'docs',
            "route": '/docs.json',
            "rule_filter": lambda rule: True,
            "model_filter": lambda tag: True,
        }],
        "static_url_path": "/flasgger_static",
        "swagger_ui": True,
        "specs_route": "/docs/"
    })
    app.register_blueprint(user_routes.user_routes, url_prefix='/user')
    app.register_blueprint(page_routes.page_routes, url_prefix='/api')
    app.register_blueprint(error_handlers.error_handler_bp)

    return app


app = create_app()
