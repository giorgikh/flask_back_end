from flask import Blueprint, request, jsonify
from app.extensions import db, jwt
from app.models.user import User
from app.schemas.user_schema import UserSchema
from flasgger import swag_from

user_routes = Blueprint('user_routes', __name__)
user_schema = UserSchema()


@user_routes.route('/register', methods=['POST'])
@swag_from(f'../static/user/register_user.yml')
def register():
    data = request.get_json()
    errors = user_schema.validate(data)
    if errors:
        return jsonify(errors), 400

    existing_user = User.query.filter_by(username=data['username']).first()
    if existing_user:
        return jsonify({"message": "Username already exists"}), 400

    user = User(username=data['username'], email=data['email'])
    user.set_password(data['password'])

    try:
        db.session.add(user)
        db.session.commit()
        return jsonify({"message": "User created successfully"}), 201
    except Exception as e:
        db.session.rollback()
        return jsonify({"message": "An error occurred while creating the user", "error": str(e)}), 500


@user_routes.route('/login', methods=['POST'])
@swag_from(f'../static/user/login_user.yml')
def login():
    data = request.get_json()
    user = User.query.filter_by(username=data['username']).first()
    if user and user.check_password(data['password']):
        token = user.encode_auth_token(user.id)
        return jsonify({'token': f"Bearer {token}"}), 200
    return jsonify({'message': 'Invalid credentials'}), 401


@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.get("id")


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User.query.filter_by(id=identity).one_or_none()
