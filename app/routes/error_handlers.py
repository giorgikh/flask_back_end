from flask import Blueprint, jsonify
from flask_jwt_extended.exceptions import NoAuthorizationError


error_handler_bp = Blueprint('error_handler', __name__)


@error_handler_bp.app_errorhandler(NoAuthorizationError)
def handle_no_authorization_error(error):
    return jsonify({'message': 'Authorization Required', 'error': str(error)}), 401


@error_handler_bp.app_errorhandler(400)
def bad_request_error(error):
    return jsonify({'message': 'Bad Request', 'error': str(error)}), 400


@error_handler_bp.app_errorhandler(403)
def forbidden_error(error):
    return jsonify({'message': 'Forbidden', 'error': str(error)}), 403


@error_handler_bp.app_errorhandler(404)
def not_found_error(error):
    return jsonify({'message': 'Not Found', 'error': str(error)}), 404


@error_handler_bp.app_errorhandler(500)
def internal_server_error(error):
    return jsonify({'message': 'Internal Server Error', 'error': str(error)}), 500
