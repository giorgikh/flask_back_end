from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask import request
from sqlalchemy import desc, asc
from sqlalchemy.exc import SQLAlchemyError
from app.extensions import db
from app.models.page import Page
from app.models.user import User
from app.schemas.page_schema import page_schema, pages_schema
from datetime import datetime
from flasgger import swag_from

page_routes = Blueprint('page_routes', __name__)


def db_operation(operation, *args, **kwargs):
    try:
        result = operation(*args, **kwargs)
        db.session.commit()
        return result, None
    except SQLAlchemyError as e:
        db.session.rollback()
        return None, str(e)


def get_current_user():
    current_user_id = get_jwt_identity()
    return User.query.get(current_user_id)


def validate_input(data, schema):
    if not data:
        return {'message': 'No input data provided'}, 400
    errors = schema.validate(data)
    if errors:
        return errors, 400
    return None


def convert_dates(data, date_fields):
    for field in date_fields:
        if field in data:
            try:
                data[field] = datetime.strptime(
                    data[field], "%Y-%m-%dT%H:%M:%S")
            except ValueError:
                return {field: 'Invalid date format'}, 400
    return None


@page_routes.route('/pages', methods=['POST'])
@jwt_required()
@swag_from(f'../static/page/create_page.yml')
def create_page():
    current_user = get_current_user()
    if not current_user:
        return jsonify({'message': 'Access forbidden'}), 403

    data = request.get_json()
    error = validate_input(data, page_schema)
    if error:
        return jsonify(error), 400

    error = convert_dates(
        data, ['create_date', 'modified_date', 'published_date'])
    if error:
        return jsonify(error), 400

    new_page = Page(**data)
    db.session.add(new_page)
    result, error = db_operation(db.session.commit)
    if error:
        return jsonify({'message': 'Database error', 'error': error}), 500
    return page_schema.jsonify(new_page), 201


@page_routes.route('/pages', methods=['GET'])
@jwt_required()
@swag_from(f'../static/page/get_pages.yml')
def get_pages():
    current_user = get_current_user()
    if not current_user:
        return jsonify({'message': 'Access forbidden'}), 403

    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 10, type=int)
    sort_by = request.args.get('sort_by', 'create_date', type=str)
    sort_order = request.args.get('sort_order', 'desc', type=str)

    sort_column = getattr(Page, sort_by, None)
    if not sort_column:
        return jsonify({'message': f'Invalid sort column: {sort_by}'}), 400

    if sort_order.lower() == 'desc':
        sort_column = desc(sort_column)
    else:
        sort_column = asc(sort_column)

    paginated_pages = Page.query.order_by(sort_column).paginate(
        page=page, per_page=per_page, error_out=False)

    result = pages_schema.dump(paginated_pages.items)

    response = {
        'pages': result,
        'total_pages': paginated_pages.pages,
        'total_items': paginated_pages.total,
        'current_page': paginated_pages.page,
        'per_page': paginated_pages.per_page
    }
    return jsonify(response), 200


@page_routes.route('/pages/<int:id>', methods=['GET'])
@jwt_required()
@swag_from(f'../static/page/get_page.yml')
def get_page(id):
    page = Page.query.get_or_404(id)
    return page_schema.jsonify(page)


@page_routes.route('/pages/<int:id>', methods=['PUT'])
@jwt_required()
@swag_from(f'../static/page/update_page.yml')
def update_page(id):
    current_user = get_current_user()
    if not current_user:
        return jsonify({'message': 'Access forbidden'}), 403

    page = Page.query.get(id)
    if not page:
        return jsonify({'message': 'Page not found'}), 404

    data = request.get_json()
    error = validate_input(data, page_schema)
    if error:
        return jsonify(error), 400

    error = convert_dates(
        data, ['create_date', 'modified_date', 'published_date'])
    if error:
        return jsonify(error), 400

    for key, value in data.items():
        setattr(page, key, value)
    page.modified_date = datetime.utcnow()
    result, error = db_operation(db.session.commit)
    if error:
        return jsonify({'message': 'Database error', 'error': error}), 500
    return jsonify({'message': 'Page Updated'}), 200


@page_routes.route('/pages/<int:id>', methods=['DELETE'])
@jwt_required()
@swag_from(f'../static/page/delete_page.yml')
def delete_page(id):
    page = Page.query.get(id)
    if not page:
        return jsonify({'message': 'Page not found'}), 404

    db.session.delete(page)
    result, error = db_operation(db.session.commit)
    if error:
        return jsonify({'message': 'Database error', 'error': error}), 500
    return jsonify({'message': 'Page Deleted Successfully'}), 204


@page_routes.route('/pages/<int:id>/activate', methods=['PUT'])
@jwt_required()
@swag_from(f'../static/page/activate_page.yml')
def activate_page(id):
    current_user = get_current_user()
    if not current_user:
        return jsonify({'message': 'Access forbidden'}), 403

    page = Page.query.get(id)
    if not page:
        return jsonify({'message': 'Page not found'}), 404

    page.status = True
    try:
        db.session.commit()
    except SQLAlchemyError as e:
        db.session.rollback()
        return jsonify({'message': 'Database error', 'error': str(e)}), 500

    return jsonify({'message': f'Page ID:{id} Activated'}), 200


@page_routes.route('/pages/<int:id>/deactivate', methods=['PUT'])
@jwt_required()
@swag_from(f'../static/page/deactive_page.yml')
def deactivate_page(id):
    current_user = get_current_user()
    if not current_user:
        return jsonify({'message': 'Access forbidden'}), 403

    page = Page.query.get(id)
    if not page:
        return jsonify({'message': 'Page not found'}), 404

    page.status = False
    try:
        db.session.commit()
    except SQLAlchemyError as e:
        db.session.rollback()
        return jsonify({'message': 'Database error', 'error': str(e)}), 500

    return jsonify({'message': f'Page ID:{id} Deactivated'}), 200
